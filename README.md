# What is Instagram Image Downloader?

InstagramImageDownloader is a utility script, coded in python used to download Instagram user's profile picture to your local machine.


# How to use the script [WIKI]

* Simply all you need to do is navigate to the script and run the following command in the terminal:

	```
	python igdp.py
	```