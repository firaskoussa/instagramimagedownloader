#!/usr/bin/env python

import os
import requests
import urllib
from bs4 import BeautifulSoup

banner = '''
  _                            _   
/_/\                          /\_\ 
\ \ \                        / / / 
 \ \ \                      / / /  
  \ \ \                    / / /   
   \ \ \                  / / /    
    \ \ \                / / /     
     \ \ \              / / /      Instagram Profile Image Downloader
      \ \ \            / / /       
       \ \ \          / / /    		By Firas Al Koussa
       _\_\/          \/_/_        
     /_/\                /\_\      
     \ \ \              / / /      
      \ \ \            / / /       
       \ \ \          / / /        
        \ \ \        / / /         
         \ \ \      / / /          
          \ \ \    / / /           
           \ \ \  / / /            
            \ \ \/ / /             
             \_\/\/_/   

'''

print banner
username = raw_input("Enter Username: ")

url = "https://www.instagram.com/"+username+"/"

conn = urllib.urlopen(url)
html = conn.read()

if "This Account is Private" in str(html):
	print "\n[==] The account is private! [==]"
	print "- I see what you're doing there stalker ;)\n"
else:
	pass


soup = BeautifulSoup(html, "lxml")
for tag in soup.findAll('img'):
    image =  str(tag['src'])
    if "ssr" in image:
    	pass
    else:
    	if "s150x150" in image:
    		print "[+] Found Profile Image!"
    		print "[+] Link: "+image
    		yesno = raw_input("\n Do you want to resize the image from 150x150 to 700x700? [y/n]: ")

    		#Check to resize
    		if yesno == 'y' or yesno == 'Y':
    			print "\n [!] Resizing image...."
	    		image = image.replace("s150x150", "m700x700")
	    		print ' [!] Downloading....'
	    		if urllib.urlretrieve(image, 'pp.jpg'):
	    			print "\n [!] Image Downloaded!"
	    		else:
	    			print "[!] Couldn't download image :("

	    	elif yesno == 'n' or yesno == 'N':
	    		print "\n [!] Downloading...."
	    		if urllib.urlretrieve(image, 'pp.jpg'):
	    			print "\n [!] Image Downloaded!"
	    		else:
	    			print "[!] Couldn't download image :("





